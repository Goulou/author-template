FROM ubuntu:22.04

RUN apt-get update && apt-get install -y --no-install-recommends \
        ca-certificates \
        ghostscript \
        libfontconfig \
        make \
        perl \
        python3-pygments \
        wget \
    && wget -qO- "https://raw.githubusercontent.com/rstudio/tinytex/v0.40/tools/install-bin-unix.sh" | sh \
    && chmod a+x /root \
    && chmod -R a+r /root/.TinyTeX \
    && chmod -R a+rx /root/.TinyTeX/bin \
    && mv /root/bin/* /usr/local/bin/. \
    && tlmgr install \
        acronym \
        algorithmicx \
        algorithms \
        babel-english \
        babel-french \
        bigfoot \
        bytefield \
        caption \
        carlisle \
        cite \
        colortbl \
        crop \
        environ \
        eurosym \
        fancyhdr \
        fnpct \
        hyphen-english \
        hyphen-french \
        koma-script \
        listings \
        makeindex \
        microtype \
        minted \
        multirow \
        nth \
        pdfcol \
        pdflscape \
        pdfpages \
        pgfplots \
        pmboxdraw \
        psnfss \
        siunitx \
        subfigure \
        tcolorbox \
        tikzfill \
        tikz-inet \
        tikz-qtree \
        translations \
        trimspaces \
        truncate \
        ucs \
        upquote \
        xstring \
    && ln -s /root/.TinyTeX/bin/x86_64-linux/makeindex /usr/local/bin/makeindex \
    && apt-get purge -y --autoremove \
        ca-certificates \
        libfontconfig \
        perl \
        wget \
    && rm -rf /var/lib/apt/lists/*

# Do not check if pygment is installed (needs RW workdir)
RUN sed -i 's!\\newcommand{\\minted@optlistcl@g}{}!\\renewcommand{\\TestAppExists}[1]{\\AppExiststrue}\n\\newcommand{\\minted@optlistcl@g}{}!g' /root/.TinyTeX/texmf-dist/tex/latex/minted/minted.sty

RUN useradd -rU user
USER user

WORKDIR /SSTIC

ENTRYPOINT ["make"]
CMD []
