SHELL := /usr/bin/env bash  # issue #3

#####################
# General Variables #
#####################

SRC=_build/actes.tmp.tex $(shell find -type f -name "*.tex" | grep "\./.*/" | grep -v "^\./_build/")
LATEX?=pdflatex -shell-escape -output-directory=_build -aux-directory=_build
LFLAGS?=-halt-on-error

GSFLAGS=-sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -dEmbedAllFonts=true -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -dSubsetFonts=true -dOptimize=true -dNOPLATFONTS -dDOPDFMARKS -dSAFER -dSTRICT -dConvertCMYKImagesToRGB=false -dProcessColorModel=/DeviceCMYK -dDetectDuplicateImages=true

BIB_MISSING = 'No file.*\.bbl|Citation.*undefined'
REFERENCE_UNDEFINED='(There were undefined references|Rerun to get (cross-references|the bars) right)'



###################
# Generic targets #
###################

.PHONY: default export clean from_docker from_docker_su


default: _build/Makefile.standalone-targets

from_docker_su: _build
	sudo docker run --rm \
		--cap-drop ALL \
		-it \
		-v ${PWD}:/SSTIC:ro \
		-v ${PWD}/_build:/SSTIC/_build:rw \
		-u `id -u`:`id -g` \
		sstic/actes ${SSTIC_TARGET}

from_docker: _build
	docker run --rm \
		--cap-drop ALL \
		-it \
		-v ${PWD}:/SSTIC:ro \
		-v ${PWD}/_build:/SSTIC/_build:rw \
		-u `id -u`:`id -g` \
		sstic/actes ${SSTIC_TARGET}
# "-v ${PWD}:/SSTIC:ro" mount the current directory inside of the container (read only)
# "-v ${PWD}/_build:/SSTIC/_build:rw" mount _build inside of the container (read/write)
# "-u `id -u`:`id -g`" ensures that files written by the container belong to the current user

export: Makefile.standalone-targets

_build:
	mkdir _build

clean:
	rm -rf _build



#######################
# Compilation helpers #
#######################

_build/%.tmp.pdf: _build/%.tmp.tex sstic.cls llncs.cls
	@rm -f $(@:.pdf=.aux) $(@:.pdf=.idx)
	$(LATEX) $(LFLAGS) $<
	bibtex $(@:.pdf=.aux) > /dev/null || true
	makeindex $(@:.pdf=.idx) > /dev/null 2> /dev/null || true
	$(LATEX) $(LFLAGS) $<
	@grep -Eqc $(BIB_MISSING) $(@:.pdf=.log) && $(LATEX) $< ; true
	@grep -Eqc $(REFERENCE_UNDEFINED) $(@:.pdf=.log) && $(LATEX) $<; true
	-grep --color '\(Warning\|Overful\).*' $(@:.pdf=.log) || true

_build/%.pdf: _build/%.tmp.pdf
	gs -sOutputFile=$@ $(GSFLAGS) $< < /dev/null > /dev/null

_build/%-online.pdf: _build/%-online.tmp.pdf
	gs -sOutputFile=$@ $(GSFLAGS) -dPrinted=false $< < /dev/null > /dev/null

%.tgz: %.pdf %
	@tar czf $@ $(@:.tgz=)/ $(@:.tgz=.pdf)
	@echo "Created $@." >&2; \



#######################
# Proceedings targets #
#######################

_build/actes-online.tmp.tex: _master.tex
	cp $< $@

_build/actes-online.tmp.pdf: _build/_articles.tex $(SRC)


_build/actes.tmp.pdf: _build/_articles.tex $(SRC)

_build/actes.tmp.tex: _master.tex _build
	@sed 's#{sstic}#[paper]{sstic}#' $< > $@



###############################
# Specific standalone targets #
###############################

_build/_articles.tex: $(SRC) Makefile _build
	@for d in [^_]*/; do \
		i=$$(basename "$$d"); \
		check_i=$$(echo "$$i" | tr -cd "a-zA-Z0-9_+-"); \
		if [ "$$i" = "$$check_i" ]; then \
			echo "\inputarticle{$$i}"; \
		fi; \
	done > $@

_build/Makefile.standalone-targets: $(SRC) Makefile _build
	@for d in [^_]*/; do \
		i=$$(basename "$$d"); \
		[ -f "$$i/master.tex" ] || continue; \
		check_i=$$(echo "$$i" | tr -cd "a-zA-Z0-9_+-"); \
		if [ "$$i" = "$$check_i" ]; then \
			echo "# Targets for $$i"; \
			echo; \
			echo "_build/$$i.tmp.tex: _standalone.tex"; \
			echo "	@sed 's#@@DIRECTORY@@#\$$(@:_build/%.tmp.tex=%)#' _standalone.tex > \$$@"; \
			echo; \
			printf "%s" "_build/$$i.tmp.pdf: _build/$$i.tmp.tex $$(echo $$i/*.tex)"; \
			ls $$i/*.bib > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/*.bib)"; \
			ls $$i/img/*.jpg > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.jpg)"; \
			ls $$i/img/*.png > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.png)"; \
			ls $$i/img/*.eps > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.eps)"; \
			ls $$i/img/*.pdf > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.pdf)"; \
			echo; \
			printf "%s" "_build/actes.tmp.pdf: _build/$$i.tmp.tex $$(echo $$i/*.tex)"; \
			ls $$i/*.bib > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/*.bib)"; \
			ls $$i/img/*.jpg > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.jpg)"; \
			ls $$i/img/*.png > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.png)"; \
			ls $$i/img/*.eps > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.eps)"; \
			ls $$i/img/*.pdf > /dev/null 2> /dev/null && printf "%s" " $$(echo $$i/img/*.pdf)"; \
			echo; \
			echo; \
			echo "$$i-clean:"; \
			echo "	rm -f _build/$$i.pdf"; \
			echo; \
			echo "default: _build/$$i.pdf"; \
			echo "clean: $$i-clean"; \
			echo "export: $$i.tgz"; \
			echo "Created targets for $$i." >&2; \
			echo; \
			echo; \
			echo; \
		else \
			echo "Ignoring invalid dir name ($$i)." >&2; \
		fi \
	done > _build/Makefile.standalone-targets

-include _build/Makefile.standalone-targets
